json.array!(@heros) do |hero|
  json.extract! hero, :id, :name, :power, :type_of, :owner, :status, :img, :cost
  json.url hero_url(hero, format: :json)
end
