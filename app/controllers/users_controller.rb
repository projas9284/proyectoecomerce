class UsersController < ApplicationController
  
  def index
    
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html {redirect_to "/users/#{@user.id}", notice: "Usuario creado exitosamente!" }
      else
        format.html {redirect_to new_user_path, notice: "Algo estuvo mal en la creacion de usuario!" } 
      end
    end
  end

  def show
    @user = User.find params[:id]
  end

  private
  def user_params
    params.require(:user).permit(:name, :email, :password)
  end
end
