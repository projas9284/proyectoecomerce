class HerosController < ApplicationController
	before_action :set_hero, only: [:show, :edit, :update, :destroy]
  def index
  	@heros= Hero.all
    @heros_json= Hero.where(owner: 1)
  	respond_to do |format|
      format.html 
      format.json {render json: @heros_json } 
    end
  end

  
  def show
    @hola= "goku2.png"
  	@hero = Hero.find(params[:id])
  	respond_to do |format|
      format.html 
      format.json {render json: @heros } 
    end
  end

 
  def new
    @hero = Hero.new
  end

 
  def edit
  	@hero = Hero.find(params[:id])
  end

  
  def create
    @hero = Hero.new(hero_params)

    respond_to do |format|
      if @hero.save
        format.html { redirect_to @hero, notice: 'hero was successfully created.' }
        format.json { render :show, status: :created, location: @hero }
      else
        format.html { render :new }
        format.json { render json: @hero.errors, status: :unprocessable_entity }
      end
    end
  end

  
  def update
  	@hero = Hero.find(params[:id])
    respond_to do |format|
      if @hero.update(hero_params)
        format.html { redirect_to @hero, notice: 'hero was successfully updated.' }
        format.json { render :show, status: :ok, location: @hero }
      else
        format.html { render :edit }
        format.json { render json: @hero.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
   @hero.destroy
    respond_to do |format|
      format.html { redirect_to heros_url, notice: 'hero was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_hero
      @hero = Hero.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def hero_params
      params.require(:hero).permit(:name, :power, :type_of, :owner, :status, :img, :cost)
    end

end
=begin string :name
      t.string :power
      t.integer :type_of
      t.boolean :owner
      t.integer :status
=end