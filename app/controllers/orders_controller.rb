class OrdersController < ApplicationController
  before_action :set_order, only: [:show]
  before_action :authenticate_user, only: [:create, :new, :show] 

  def index
    @orders= Order.all
    respond_to do |format|
      format.html 
      format.json {render json: @orders } 
    end
  end

  
  def show
    @order = Order.find(params[:id])
    @heros_a
    respond_to do |format|
      format.html 
      format.json {render json: @orders } 
    end
  end

 
  def new
    @order = Order.new
  end

  # def edit
  #   @order = Order.find(params[:id])
  # end
  def external 
    # @text = params[:name]

    # render json: @text
    @order = Order.new
    @names = params[:name]
    @order.amount = 0
    @names.each do |name|
      hero = Hero.find_by_name(name)
      @order.heros.push hero
      @order.amount = @order.amount + hero.cost
    end
    @order.user_id = 5
    @order.save
    render json: @order
  end


  def create
    @order = Order.new
    @user = session[:user_id]
    @order.user_id = @user
    @order.amount = 0
    @heros_a = []
    @array_ids = params[:hero]
    @array_ids.each do |id|
      loco = Hero.find(id)
      if loco.owner == 0
        @heros_a.push loco.name
      end   
      @order.heros.push loco
      @order.amount = @order.amount + loco.cost
    end
    @order.team_a_order @heros_a
    respond_to do |format|
      if @order.save
        format.html { redirect_to @order, notice: 'Order was successfully created.' }
        format.json { render :show, status: :created, location: @order }
      else
        format.html { render :new }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # def update
  #   @order = Order.find(params[:id])
  #   respond_to do |format|
  #     if @order.update(order_params)
  #       format.html { redirect_to @order, notice: 'order was successfully updated.' }
  #       format.json { render :show, status: :ok, location: @order }
  #     else
  #       format.html { render :edit }
  #       format.json { render json: @order.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  def destroy
    @order = Order.find(params[:id])
    @order.destroy
    respond_to do |format|
      format.html { redirect_to orders_url, notice: 'Oder was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def order_params
      params.require(:order).permit(:hero,:name)
    end

end