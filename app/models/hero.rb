class Hero < ActiveRecord::Base
	enum owner: ['team_a', 'team_b']
	enum status: ['available', 'not_available']
	has_and_belongs_to_many :orders
end
