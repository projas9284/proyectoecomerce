class ChangeColumnHeroOwner < ActiveRecord::Migration
  def change
  	change_column :heros, :owner, :integer
  end
end
