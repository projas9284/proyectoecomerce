class AddImgCostToHero < ActiveRecord::Migration
  def change
    add_column :heros, :img, :string
    add_column :heros, :cost, :float
  end
end
